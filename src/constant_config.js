const SERV_WWS =
  "ws://node-env.eba-7xwegkmr.ap-southeast-1.elasticbeanstalk.com:8080";

const SERV_API = "http://cc-srv-api.sd-digital.link/api/v2.0/";

export { SERV_WWS, SERV_API };
