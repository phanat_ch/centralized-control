import React, { useState, useEffect, useRef, useContext } from "react";
import { Row, Col, Table, Modal, Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import Pagination from "../components/pagination";
import jwt_decode from "jwt-decode";
import { MqttContext } from "../components/mqtt_context";
import { products } from "../api/product";

import Building from "../assets/image/dashboard/floor-demo.png";
import Upload from "../assets/image/setting/upload.png";

const CompanyList = () => {
  const [companyList, setCompanyList] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [PerPage] = useState(10);
  const [show, setShow] = useState(false);
  const [addForm, setAddForm] = useState(false);
  const [editForm, setEditForm] = useState(false);
  const [deleteForm, setDeleteForm] = useState(false);
  const [modalImage, setModalImage] = useState(null);
  const [companySearch, setCompanySearch] = useState("");
  const [companyListSearch, setCompanyListSearch] = useState("");
  const [deleteModal, setDeleteModal] = useState(false);
  const [validated, setValidated] = useState(false);
  const [company, setCompany] = useState("");
  const [companyImage, setCompanyImage] = useState(null);
  const [validateImage, setValidateImage] = useState(true);

  const indexOfLastPost = currentPage * PerPage;
  const indexOfFirstPost = indexOfLastPost - PerPage;
  const currentPosts = companyListSearch.slice(
    indexOfFirstPost,
    indexOfLastPost
  );

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const uploadedImage = useRef(null);
  const imageUploader = useRef(null);

  const { connect_socket, disconnect_socket, setProductList } =
    useContext(MqttContext);

  let navigate = useNavigate();

  useEffect(() => {
    const company_list = [
      {
        id: "61d7add670710000f9005d23",
        company_name: "โรงพยาบาล 1",
        company_image: Building,
      },
      {
        id: "624e60b11f4965336c6306a0",
        company_name: "โรงพยาบาล 2",
        company_image: Building,
      },
      {
        id: 3,
        company_name: "โรงพยาบาล 3",
        company_image: Building,
      },
      {
        id: 4,
        company_name: "โรงพยาบาล 4",
        company_image: Building,
      },
      {
        id: 5,
        company_name: "โรงพยาบาล 5",
        company_image: Building,
      },
      {
        id: 6,
        company_name: "โรงพยาบาล 6",
        company_image: Building,
      },
    ];

    setCompanyList(company_list);
    setCompanyListSearch(company_list);
  }, []);

  useEffect(() => {
    if (localStorage.getItem("token")) {
      let decoded = jwt_decode(localStorage.getItem("token"));
      let exp = parseInt(decoded.exp);
      let now = parseInt(moment().format("X"));

      if (decoded.role !== "1" && exp > now) {
        navigate("/");
      }
    } else {
      navigate("/login");
    }
  }, [navigate]);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (!companyImage) {
      setValidateImage(false);
    }

    setValidated(true);
  };

  const handleImageUpload = (e) => {
    const [file] = e.target.files;
    if (file) {
      const reader = new FileReader();
      const { current } = uploadedImage;
      current.file = file;
      reader.onload = (e) => {
        current.src = e.target.result;
      };
      reader.readAsDataURL(file);

      setValidateImage(true);
      setCompanyImage(e.target.files[0]);
    }
  };

  const search = () => {
    var result = companyList.filter((item) => {
      if (item.company_name.toLowerCase().indexOf(companySearch) >= 0) {
        return true;
      } else {
        return false;
      }
    });

    setCompanyListSearch(result);
  };

  const gotoCompany = async (company_id) => {
    localStorage.setItem("company_id", company_id);

    disconnect_socket();

    try {
      let token = localStorage.getItem("token");
      let res = await products(token);

      let productArr = res.data.productsList.filter((item) => {
        if (item.company_id === localStorage.getItem("company_id")) {
          return true;
        }

        return item;
      });

      setProductList(productArr);
      connect_socket(productArr);

      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="main-bg pb-2">
      <div className="body-container w-100 ms-0">
        <Row className="mx-0 p-4">
          <Col xs={12} className="pe-0">
            <div className="table-container p-4">
              <div
                className="table-container-title px-2"
                style={{ justifyContent: "space-between" }}
              >
                <h5 className="mb-1">Company List</h5>
                <button
                  className="add-modal-btn"
                  onClick={() => {
                    setCompany("");
                    setCompanyImage(null);
                    setAddForm(true);
                  }}
                >
                  Add Company
                </button>
              </div>
              <Row>
                <Col xs={3} xxl={2} className="pe-0">
                  <Form.Group className="mt-4 mb-2 px-2">
                    <Form.Label className="input-label">
                      Company Name
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Company"
                      required
                      onChange={(e) => setCompanySearch(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mt-4 mb-2 px-2">
                    <Form.Label className="input-label mb-0">&nbsp;</Form.Label>
                  </Form.Group>
                  <button className="search-btn" onClick={() => search()}>
                    Search
                  </button>
                </Col>
              </Row>
              <div className="px-2">
                <Table hover className="mt-3 setting-table align-middle">
                  <thead>
                    <tr>
                      <th className="px-4">No.</th>
                      <th className="text-center px-4">Image</th>
                      <th className="text-center px-4">Company Name</th>
                      <th className="text-center px-4">View</th>
                      <th className="text-center px-4">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {companyListSearch.length ? (
                      currentPosts.map((item, index) => (
                        <tr key={index}>
                          <td className="px-4">
                            {currentPage === 1
                              ? index + 1
                              : index + 1 + (currentPage - 1) * PerPage}
                            .
                          </td>
                          <td className="px-4 text-center" width={"20%"}>
                            <img
                              src={item.company_image}
                              alt="Company"
                              className="table-image py-2"
                              onClick={() => {
                                setModalImage(item.company_image);
                                setShow(true);
                              }}
                            />
                          </td>
                          <td className="px-4 text-center" width={"40%"}>
                            {item.company_name}
                          </td>
                          <td className="px-4 text-center">
                            <span
                              className="table-manage"
                              onClick={() => gotoCompany(item.id)}
                            >
                              View
                            </span>
                          </td>
                          <td className="px-4 text-center">
                            <span
                              className="table-edit"
                              onClick={() => {
                                setCompany(item.company_name);
                                setModalImage(item.company_image);
                                setEditForm(true);
                              }}
                            >
                              Edit
                            </span>{" "}
                            /{" "}
                            <span
                              className="table-delete"
                              onClick={() => setDeleteModal(true)}
                            >
                              Delete
                            </span>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colSpan={5} className="text-center">
                          No Data
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </div>
              <Pagination
                postsPerPage={PerPage}
                totalPosts={companyListSearch.length}
                paginate={paginate}
                currentPage={currentPage}
              />
            </div>
          </Col>
        </Row>
      </div>
      <Modal size="lg" show={show} onHide={() => setShow(false)} centered>
        <div className="position-relative">
          <img src={modalImage} alt="modal" className="w-100 h-auto" />
          <button
            type="button"
            className="btn-close modal-close-btn"
            aria-label="Close"
            onClick={() => setShow(false)}
          />
        </div>
      </Modal>
      <Modal show={editForm} onHide={() => setEditForm(false)} centered>
        <Modal.Body>
          <h4 className="modal-title mb-2">Edit Building</h4>
          <Form
            noValidate
            validated={validated}
            onSubmit={(e) => handleSubmit(e, "edit")}
          >
            <Form.Group className="mb-4 px-2">
              <Form.Label className="input-label">Company Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Company"
                defaultValue={company}
                required
                onChange={(e) => setCompany(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid company name.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-4 px-2">
              <Form.Label className="input-label">Company Image</Form.Label>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <input
                  type="file"
                  accept="image/*"
                  onChange={handleImageUpload}
                  ref={imageUploader}
                  style={{
                    display: "none",
                  }}
                />
                <div
                  className="form-image-container mb-2"
                  onClick={() => imageUploader.current.click()}
                >
                  <img
                    ref={uploadedImage}
                    src={modalImage}
                    className="form-image-input"
                    alt="profile"
                  />
                </div>
              </div>
            </Form.Group>
            <div className="modal-btn-container mb-2">
              <Button
                type="reset"
                onClick={() => {
                  setEditForm(false);
                  setValidateImage(true);
                  setValidated(false);
                }}
              >
                Cancel
              </Button>
              <Button type="submit">Save</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      <Modal show={addForm} onHide={() => setAddForm(true)} centered>
        <Modal.Body>
          <h4 className="modal-title mb-2">Add Building</h4>
          <Form
            noValidate
            validated={validated}
            onSubmit={(e) => handleSubmit(e, "add")}
          >
            <Form.Group className="mb-4 px-2">
              <Form.Label className="input-label">Company Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Company"
                required
                onChange={(e) => setCompany(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid company name.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-4 px-2">
              <Form.Label className="input-label">Company Image</Form.Label>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <input
                  type="file"
                  accept="image/*"
                  onChange={handleImageUpload}
                  ref={imageUploader}
                  style={{
                    display: "none",
                  }}
                  required
                />
                <div
                  className={
                    validateImage
                      ? "form-image-container"
                      : "form-image-container error-image-upload"
                  }
                  onClick={() => imageUploader.current.click()}
                >
                  <img
                    ref={uploadedImage}
                    src={Upload}
                    className="form-image-input"
                    alt="Company"
                  />
                </div>
                <p
                  className="form-error mb-0"
                  style={{ display: validateImage ? "none" : "block" }}
                >
                  Please provide a company image.
                </p>
              </div>
            </Form.Group>
            <div className="modal-btn-container mb-2">
              <Button
                type="reset"
                onClick={() => {
                  setAddForm(false);
                  setValidateImage(true);
                  setValidated(false);
                }}
              >
                Cancel
              </Button>
              <Button type="submit">Save</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      <Modal
        show={deleteForm}
        onHide={() => setDeleteForm(true)}
        centered
      ></Modal>
      <Modal show={deleteModal} centered>
        <Modal.Body>
          <h4 className="modal-title mb-4">
            Do you want delete this company ?
          </h4>
          <div className="modal-btn-container mb-2">
            <Button
              type="button"
              onClick={() => {
                setDeleteModal(false);
              }}
            >
              Cancel
            </Button>
            <Button
              type="button"
              onClick={() => {
                setDeleteModal(false);
              }}
            >
              Save
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default CompanyList;
