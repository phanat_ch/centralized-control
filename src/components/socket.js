import io from "socket.io-client";
import { SERV_WWS } from "../constant_config";

let socket;

const initiateSocket = () => {
  //   socket = io(SERV_WWS, {
  //     query: {
  //       token: localStorage.getItem("token"),
  //     },
  //     reconnectionDelay: 2500,
  //     reconnectionAttempts: 10,
  //     reconnection: false,
  //   });

  socket = io(
    "wss://demo.piesocket.com/v3/channel_123?api_key=VCXCEuvhGcBDP7XhiJJUDvR1e1D3eiVjgZ9VRiaV&notify_self"
  );

  socket.on("connect", () => {
    console.log("Connected");
  });

  socket.on("error", (e) => {
    console.log(e.message);
  });
};

const disconnectSocket = () => {
  if (socket) {
    socket.disconnect();
    socket.off("connect");
    socket.off("disconnect");
    socket.off("products/" + localStorage.getItem("company_id"));
    console.log("disconnect socket!");
  }
};

const subscribe = (cb) => {
  if (!socket) return true;

  socket.on("products/" + localStorage.getItem("company_id"), (data) => {
    return cb(null, data);
  });
};

export { initiateSocket, disconnectSocket, subscribe };
